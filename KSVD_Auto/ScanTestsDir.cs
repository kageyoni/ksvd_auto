﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using IniParser;
using IniParser.Model;

namespace KSVD_Auto
{
    public partial class MainWindow : Window
    {
        public void ScanTestsDir()
        {
            string[] finded_scripts = Directory.GetFiles(scriptsDir, "*.au3"); // список всех .au3 файлов в директории path_to_scripts

            if (finded_scripts.Length < 1)
            {
                testList.Clear();// Очищаем ранее построенный список, если такой был
                tests_data_grid.Items.Refresh();
                MessageBox.Show("Директория не содержит скриптов .au3");

            }
            else if (finded_scripts != null)
            {

                testList.Clear(); // Очищаем ранее построенный список, если такой был
                tests_data_grid.Items.Refresh(); // Обновляем визуальное отображение списка

                for (int i = 0; i < (finded_scripts.Length); i++) // Получаем список имён тестов, для заполнения поля выбора тестов
                {
                    int y = i + 1;
                    string full_path = finded_scripts[i];
                    finded_scripts[i] = finded_scripts[i].Substring(scriptsDir.Length + 1); //Обрезаем путь до скрипта в названии теста
                    finded_scripts[i] = finded_scripts[i].TrimEnd(new char[] { '.', 'a', 'u', '3' }); //Обрезаем расширение скрипта
                    string config_file_name = scriptsDir + "\\" + finded_scripts[i] + ".ini";

                    bool isTestRemote;
                    string executeType;
                    if (File.Exists(config_file_name))
                    {
                        var parser = new FileIniDataParser();
                        IniData data = parser.ReadFile(config_file_name);
                        isTestRemote = Convert.ToBoolean(data["TestConfig"]["isTestRemote"]);
                    }
                    else
                    {
                        isTestRemote = false;
                    }
                    if (isTestRemote == true)
                    {
                        executeType = "Remote";
                    }
                    else
                    {
                        executeType = "Local";
                    }

                    testList.Insert(i, new TestList { CheckBox = false, Order = y, Name = finded_scripts[i], Status = "", FullPath = full_path, Msg = "", ExecuteType = executeType }); //наполняем список тестов
                    y++;
                }
                tests_data_grid.ItemsSource = testList; //Привязываем DataGrid к источнику
                tests_data_grid.Items.Refresh(); // Обновляем визуальное отображение списка
                export_btn.IsEnabled = true;

                if (tests_data_grid.Items.Count == 0)
                {
                    delete_btn.IsEnabled = false;
                    export_btn.IsEnabled = false;
                }
            }
        }
    }
}
