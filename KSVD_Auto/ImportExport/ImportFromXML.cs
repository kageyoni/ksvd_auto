﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace KSVD_Auto
{
    public class ImportFromXML
    {
        string filename;

        public ImportFromXML(string filename)
        {
            this.filename = filename;
        }

        public List<TestList> DoImportFromXML()
        {
            List<TestList> testList = new List<TestList>();
            XDocument doc = XDocument.Load(filename);
            var q = doc.Descendants("Item").ToList();

            for (int i = 0; i < q.Count; i++)
            {
                string tmp;
                int order;
                string name;
                string fullpath;
                //bool 

                tmp = Convert.ToString(q[i].Element("Name"));
                name = tmp.Substring(6, tmp.Length - 13);
                tmp = Convert.ToString(q[i].Element("Order"));
                tmp = tmp.Substring(7, tmp.Length - 15);
                order = Convert.ToInt32(tmp);
                tmp = Convert.ToString(q[i].Element("FullPath"));
                fullpath = tmp.Substring(10, tmp.Length - 21);
                //переделать импорт с учётом нового типа работы со строками + с учётом нового параметра списка тестов - выполняется ли тест локально или удалённо
                //tmp = Convert.ToString(q[i].Element("AgentStatus"));
                //tmp = tmp.Substring(tmp.IndexOf(">") + 1);
                //agentStatus = tmp.Remove(tmp.IndexOf("<"));
                testList.Insert(i, new TestList { Order = order, Name = name, FullPath = fullpath }); //наполняем список тестов из XML
            }
            return testList;
        }
    }
}
