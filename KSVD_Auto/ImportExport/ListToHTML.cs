﻿using System.Collections.Generic;
using System.Text;

namespace KSVD_Auto
{
    public class ListToHTML
    {
        int colunms_count;
        string[] columns_header;
        List<TestList> testList;

        public ListToHTML(int colunms_count, string[] columns_header, List<TestList> testList)
        {
            this.colunms_count = colunms_count;
            this.columns_header = columns_header;
            this.testList = testList;

        }

        public string  StartConvertListToHTML ()
            {
            string html_body = ConvertListToHTML().ToString();
            return html_body;
            }

        StringBuilder ConvertListToHTML()
        {
            StringBuilder strB = new StringBuilder();
            //create html & table
            strB.AppendLine("<html><body><center><" +
                          "table border='1' cellpadding='0' cellspacing='0'>");
            strB.AppendLine("<tr>");
            //cteate table header
            for (int i = 1; i < colunms_count; i++)
            {
                strB.AppendLine("<td align='center' valign='middle'>" +
                               columns_header[i] + "</td>");
            }
            //create table body
            strB.AppendLine("<tr>");
            for (int i = 0; i < testList.Count; i++)
            {
                strB.AppendLine("<tr>");
                strB.AppendLine("<td align='center' valign='middle'>" +
                testList[i].Order + "</td>");
                strB.AppendLine("<td align='center' valign='middle'>" +
                testList[i].Name + "</td>");
                strB.AppendLine("<td align='center' valign='middle'>" +
                testList[i].Status + "</td>");
                strB.AppendLine("<td align='center' valign='middle'>" +
                testList[i].Msg + "</td>");
            }
            //table footer & end of html file
            strB.AppendLine("</table></center></body></html>");
            return strB;
        }
    }
}
