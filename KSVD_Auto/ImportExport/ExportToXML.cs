﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace KSVD_Auto
{
    public class ExportToXML
    {
        List<TestList> testList;
        string filename;

        public ExportToXML(List<TestList> testList, string filename)
        {
            this.testList = testList;
            this.filename = filename;
        }

        public void DoExportToXML()
        {
            var xEle = new XElement("testList",
            from xml in testList
            select new XElement("Item",
                           new XElement("Order", xml.Order),
                           new XElement("Name", xml.Name),
                           new XElement("FullPath", xml.FullPath)
                       ));
            try
            {
                xEle.Save(filename);
            }
            catch
            {
                System.Windows.MessageBox.Show("Возникло исключение!");
            }
        }
    }
}