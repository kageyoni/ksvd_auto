﻿using System.IO;
using System.Windows;
using System.Windows.Forms;

namespace KSVD_Auto
{
    public partial class MainWindow : Window
    {
        private void import_btn_Click(object sender, RoutedEventArgs e)
        {
            if (System.Windows.MessageBox.Show("Заменить текущий список тестов?", "Импорт из XML", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
                //do no stuff
            }
            else
            {
                //do yes stuff
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "XML files(*.xml)|*.xml";
                ofd.ShowDialog();
                string filename = ofd.FileName;
                var test = testList;
                testList.Clear(); // Очищаем ранее построенный список, если такой был
                if (filename == "") //Если строка пуста
                {
                }
                else //Если строка заполнена
                {
                    ImportFromXML import_from_xml = new ImportFromXML(filename);
                    testList = import_from_xml.DoImportFromXML();
                    string tmp = testList[0].FullPath;
                    scriptsDir = tmp.Remove(tmp.LastIndexOf('\\')); //Удаляем все символы с конца до первого \\ включительно
                    tests_path_field.Text = scriptsDir;
                    tests_data_grid.ItemsSource = testList; //Привязываем DataGrid к источнику
                    tests_data_grid.Items.Refresh(); // Обновляем визуальное отображение списка
                    export_btn.IsEnabled = true;
                    export_html_btn.IsEnabled = false;
                }
            }
        }

        private void export_btn_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "XML files(*.xml)|*.xml";
            sfd.ShowDialog();
            string filename = sfd.FileName;

            if (filename == "") //Если строка пуста
            {
            }
            else //Если строка заполнена
            {
                ExportToXML export_to_xml = new ExportToXML(testList, filename);
                export_to_xml.DoExportToXML();
            }
        }

        private void export_html_btn_Click(object sender, RoutedEventArgs e) //Экспортирование результатов прогона в html
        {

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "HTML files(*.html)|*.html";
            sfd.ShowDialog();
            string filename = sfd.FileName;

            if (filename == "") //Если строка пуста
            {
            }
            else //Если строка заполнена
            {
                string[] columns_header = new string[tests_data_grid.Columns.Count]; 

                for (int i = 0; i < tests_data_grid.Columns.Count; i++) //Заполняем массив значениями заголовков столбцов
                {
                    columns_header[i] = tests_data_grid.Columns[i].Header.ToString();
                }

                ListToHTML list_to_html = new ListToHTML(tests_data_grid.Columns.Count, columns_header, testList);
                string html_body = list_to_html.StartConvertListToHTML();
                File.WriteAllText(@filename, html_body);
            }
        }
    }
}
