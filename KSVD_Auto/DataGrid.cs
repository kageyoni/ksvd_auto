﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace KSVD_Auto
{
    public partial class MainWindow : Window
    {

        void myIsActiveCheckBox_Click(object sender, RoutedEventArgs e) //Обрабатываем изменение статуса чекбокса
        {
            int selected_row = tests_data_grid.SelectedIndex;


            if (testList[selected_row].CheckBox == false) // меняем статус чекбокса в зависимости от клика
            {
                testList[selected_row].CheckBox = true;
            }
            else
            {
                testList[selected_row].CheckBox = false;
            }

            for (int i = 0; i < tests_data_grid.Items.Count; i++) // меняем активность кнопок "удалить" и "Запустить прогон" в зависимости от статусов чекбоксов
            {
                if (testList[i].CheckBox == true)
                {
                    delete_btn.IsEnabled = true;
                    start_btn.IsEnabled = true;
                    break;
                }
                else
                {
                    delete_btn.IsEnabled = false;
                    start_btn.IsEnabled = false;
                }
            }
        }

        private void tests_data_grid_SelectionChanged(object sender, SelectionChangedEventArgs e) //Обработка активности кнопок "Вверх"/"Вниз" в зависимости от выбранного поля (не чекбокс, а выделение)
        {
            selected_row = tests_data_grid.SelectedIndex;

            if (selected_row != 0 && selected_row == (tests_data_grid.Items.Count - 1))
            {
                up_btn.IsEnabled = true;
                down_btn.IsEnabled = false;
            }
            else if (selected_row == 0 && selected_row != (tests_data_grid.Items.Count - 1))
            {
                up_btn.IsEnabled = false;
                down_btn.IsEnabled = true;
            }
            else if (selected_row != (tests_data_grid.Items.Count - 1) && selected_row != 0)
            {
                up_btn.IsEnabled = true;
                down_btn.IsEnabled = true;
            }
            else if (selected_row == (tests_data_grid.Items.Count - 1) && selected_row == 0)
            {
                up_btn.IsEnabled = false;
                down_btn.IsEnabled = false;
            }
        }
        private void up_btn_Click(object sender, RoutedEventArgs e) //Кнопка "Вверх"
        {
            int temp_index = selected_row;
            var temp_item = testList[temp_index - 1];
            testList.RemoveAt(temp_index - 1);
            testList.Insert(temp_index, temp_item);
            testList[temp_index - 1].Order--;
            testList[temp_index].Order++;
            selected_row--;
            if (temp_index - 1 == 0)
            {
                up_btn.IsEnabled = false;
            }
            else
            {
                down_btn.IsEnabled = true;
            }
            ResetChekedStatus();
            tests_data_grid.Items.Refresh(); // Обновляем визуальное отображение списка
        }

        private void down_btn_Click(object sender, RoutedEventArgs e) //Кнопка "Вниз"
        {
            int temp_index = selected_row;
            var temp_item = testList[temp_index + 1];
            testList.RemoveAt(temp_index + 1);
            testList.Insert(temp_index, temp_item);
            testList[temp_index + 1].Order++;
            testList[temp_index].Order--;
            selected_row++;
            if (temp_index + 2 == testList.Count)
            {
                down_btn.IsEnabled = false;
                
            }
            else
            {
                up_btn.IsEnabled = true;
            }
            ResetChekedStatus();
            tests_data_grid.Items.Refresh(); // Обновляем визуальное отображение списка
        }

        private void delete_btn_Click(object sender, RoutedEventArgs e) //Кнопка "Удалить"
        {
            for (int i = 0; i < tests_data_grid.Items.Count; i++)
            {
                if (testList[i].CheckBox == true)
                {
                    testList.RemoveAt(i);

                    for (int x = i; x < tests_data_grid.Items.Count; x++)
                    {
                        testList[x].Order--;
                    }

                    i = i - 1;
                }
            }
            tests_data_grid.Items.Refresh();
            delete_btn.IsEnabled = false;
            if (tests_data_grid.Items.Count == 0)
            {
                export_btn.IsEnabled = false;
            }
        }
        private void ResetChekedStatus() //Сброс параметра CheckBox для каждого теста в списке
        {
            start_btn.IsEnabled = false;
            for (int i = 0; i < tests_data_grid.Items.Count; i++)
            {
                testList[i].CheckBox = false;
            }
        }
    }
}
