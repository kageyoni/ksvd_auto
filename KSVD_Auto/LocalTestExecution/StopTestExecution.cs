﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSVD_Auto
{
    partial class LocalTestExecution
    {
        public void StopTestExecution()
        {
            var logger = NLog.LogManager.GetCurrentClassLogger();

            try
            {
                foreach (Process proc in Process.GetProcessesByName("AutoIt3")) //Ищем и убиваем процесс AutoIt3
                {
                    proc.Kill();
                    logger.Info("Выполнение локального теста " + testName + " остановлено");
                }
            }
            catch (Exception ex)
            {
                testStatusInner = "StopFailed";
                logger.Error("Не удалось остановить процесс AutoIT. " + ex);
            }
        }
    }
}
