﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KSVD_Auto
{
    partial class LocalTestExecution
    {
        bool stopTestExecution = false;
        int time;
        string fullPath;
        string scriptsDir;
        string scritpLogsDir;
        string testName;
        string testStatusInner = "";
        //string testStatusInner = "";
        bool stepSuccess;
        string successFile;
        string failFile;

        public LocalTestExecution(string scriptsDir, string fullPath, string testName, int time)
        {
            this.scriptsDir = scriptsDir;
            this.fullPath = fullPath;
            this.testName = testName;
            this.time = time;
        }

        public void DoLocalTestExecution(out string testStatus, out string testMsg)
        {
            var logger = NLog.LogManager.GetCurrentClassLogger();
            logger.Info("Выполняется подготовка к запуску теста: " + testName);
            testStatus = "";
            testMsg = "";
            scritpLogsDir = scriptsDir + "\\Results\\" + testName;
            successFile = scritpLogsDir + "\\success.txt";
            failFile = scritpLogsDir + "\\fail.txt";
            

            int step = 1;
            int innerTime = 0;
            while (stopTestExecution == false && innerTime != time)
            {
                logger.Debug("Step: " + step);
                stopTestExecution = Properties.Settings.Default.StopTestsExecution; //Проверка, не нужно ли отменить выполнение теста
                if (stopTestExecution == false && step == 1) //Шаг 1. Запуск теста
                {
                    stepSuccess = false;
                    RunTestScriptFile(); //Запускаем тест
                    if (stepSuccess == true)
                    {
                        step++;
                    }
                    else if (stepSuccess == false)
                    {
                        testStatus = "Failed";
                        testMsg = "Не удалось запустить тестовый скрипт";
                        break;
                    }
                }
                else if (stopTestExecution == false && step == 2) //Шаг 2. Проверка создания директории результатов выполнения теста
                {
                    stepSuccess = false;
                    CheckTestLogsDir();
                    if (stepSuccess == true)
                    {
                        step++;
                    }
                    else if (stepSuccess == false)
                    {
                        if (innerTime == 5) //5 попыток по секунде на проверку создания директории
                        {
                            testStatus = "Failed";
                            testMsg = "Не найдена директория результатов скрипта";
                            break;
                        }
                    }
                }
                else if (stopTestExecution == false && step == 3) //Шаг 3. Проверка результатов выполнения теста
                {
                    stepSuccess = false;
                    CheckTestExecutionStatus();

                    if (stepSuccess == true)
                    {
                        testStatus = "OK";
                        testMsg = "Тест успешно пройден";
                        break;
                    }
                    //else if (stepSuccess == false)
                    //{
                    //    testStatus = "Failed";
                    //    testMsg = "Не найдена директория результатов скрипта";
                    //}
                    else if (testStatusInner == "Failed")
                    {
                        testStatus = "Failed";
                        testMsg = "Тест завершился с ошибкой";
                        break;
                    }
                }

                else if (stopTestExecution == true)
                {
                    StopTestExecution();

                    if (testStatusInner == "StopFailed")
                    {
                        testStatus = testStatusInner;
                        testMsg = "Произошла ошибка при остановке теста";
                    }
                    else if (testStatusInner != "StopFailed")
                    {
                        testStatus = "Stopped";
                        testMsg = "Выполнение теста остановлено";
                    }      
                    break;
                }
                Thread.Sleep(1000);
                innerTime++;
                if (innerTime == time)
                {
                    StopTestExecution();
                    if (testStatusInner == "StopFailed")
                    {
                        testStatus = "Timeout";
                        testMsg = "Тест не был выполнен за отведённое время. Произошла ошибка при остановке теста";
                    }
                    else
                    {
                        testStatus = "Timeout";
                        testMsg = "Тест не был выполнен за отведённое время. Тест остановлен";
                    }
                }
            }
        }
    }
}
