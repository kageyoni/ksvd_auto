﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSVD_Auto
{
    partial class LocalTestExecution
    {
        public void CheckTestExecutionStatus()
        {
            var logger = NLog.LogManager.GetCurrentClassLogger();

                if (File.Exists(successFile))
                {
                    stepSuccess = true;
                }
                else if (File.Exists(failFile))
                {
                    testStatusInner = "Failed";
                }
                else
                {
                    stepSuccess = false;
                }
        }
    }
}
