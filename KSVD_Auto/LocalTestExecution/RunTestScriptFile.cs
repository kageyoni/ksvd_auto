﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSVD_Auto
{
    partial class LocalTestExecution
    {
        void RunTestScriptFile()
        {
            var logger = NLog.LogManager.GetCurrentClassLogger();
            logger.Info("Выполняется запуск теста: " + fullPath);

            try
            {
                Process.Start(fullPath); //Запуск файла .au3. В винде должен установлен AutoIT иначе скрипт может открыться в другой программе или не открыться вовсе
                stepSuccess = true;
            }
            catch (Exception ex)
            {
                stepSuccess = false;
                logger.Error("Произошла ошибка запуска файла. Exeption: " + ex);
            }
        }
    }
}
