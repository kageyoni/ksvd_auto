﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.IO;
using MessageBox = System.Windows.MessageBox;
using System.Data;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Threading;
using System.Xml.Linq;
using System.Net.Sockets;
using System.Net;

namespace KSVD_Auto
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            tests_data_grid.SelectionChanged += tests_data_grid_SelectionChanged;
            version_info.Text = "Ver. " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();        }

        string scriptsDir;
        Thread executingTread;
        int selected_row;
        int time;
        


        List<TestList> testList = new List<TestList> { }; //Инициализируем список тестов для заполнения таблицы

        private void browse_btn_Click(object sender, RoutedEventArgs e) //Кнопка "Обзор"
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowDialog();
            tests_path_field.Text = fbd.SelectedPath;
            scriptsDir = tests_path_field.Text;
        }

        private void start_btn_Click(object sender, RoutedEventArgs e) //Кнопка "Запуск прогона"
        {
            start_btn.IsEnabled = false;
            cancel_btn.IsEnabled = true;
            time = Convert.ToInt32(time_field.Text);
            executingTread = new Thread(() => ExecutingRun(time));
            executingTread.Start();
        }

        private void scan_btn_Click(object sender, RoutedEventArgs e) //Кнопка "Сканировать"
        {
            up_btn.IsEnabled = false;
            down_btn.IsEnabled = false;
            export_html_btn.IsEnabled = false;

            if (scriptsDir == null || scriptsDir == "")
            {
                MessageBox.Show("Укажите путь до директории скриптов");
            }
            else if (scriptsDir != null || scriptsDir != "")
            {
                ScanTestsDir();
            }
        }

        private void cancel_btn_Click(object sender, RoutedEventArgs e) //Кнопка "Отмена"
        {

            Properties.Settings.Default.StopTestsExecution = true;
            cancel_btn.IsEnabled = false;



            //MessageBox.Show("Выполнение прогона остановлено");
        }
       
    }
}
