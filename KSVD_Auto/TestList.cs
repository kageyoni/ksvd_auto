﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KSVD_Auto
{
    public class TestList
    {
        public bool CheckBox { get; set; }
        public int Order { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string FullPath { get; set; }
        public string Msg { get; set; }
        public bool Partialtest { get; set; }
        public string ExecuteType { get; set; }
        public string OS { get; set; }
    }
}
