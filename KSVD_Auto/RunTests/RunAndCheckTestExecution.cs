﻿using System;
using System.Diagnostics;
using System.IO;

namespace KSVD_Auto
{
    public class RunAndCheckTestExecution
    {

        string scritp_logs_dir;
        string success_file;
        string fail_file;

        public RunAndCheckTestExecution(string scritp_logs_dir, string success_file, string fail_file)
        {
            this.scritp_logs_dir = scritp_logs_dir;
            this.success_file = success_file;
            this.fail_file = fail_file;
        }

        public void RunTests(string full_path, int time, out string status, out string msg)
        {
            var logger = NLog.LogManager.GetCurrentClassLogger();
            logger.Info("Выполняется запуск теста: " + full_path);
            status = "";
            msg = "";

            try
            {
                Process.Start(full_path); //Запуск файла .au3. В винде должен установлен AutoIT иначе скрипт может открыться в другой программе или не открыться вовсе
                status = "OK";
            }
            catch (Exception ex)
            {
                status = "Failed";
                msg = "Произошла ошибка запуска файла";
                logger.Error("Произошла ошибка запуска файла. Exeption: " + ex);
            }

            if (status == "OK")
            {
                System.Threading.Thread.Sleep(1000);
            }
            else
            {
                msg = "Произошла ошибка запуска файла";
            }
        }

        public void CheckSuccessFileAndFailFile(int time, out string msg, out string status)
        {
            var logger = NLog.LogManager.GetCurrentClassLogger();
            status = "";
            msg = "";

            for (int i = 0; i < time; i++) //Проверяем результат завершения теста
            {
                if (File.Exists(success_file))
                {
                    msg = "Тест успешно пройден";
                    status = "OK";
                    break;
                }
                else if (File.Exists(fail_file))
                {
                    msg = "Произошла ошибка в выполнении теста";
                    status = "Failed";
                    break;
                }
                else
                {
                    msg = "Идёт выполнение";
                    System.Threading.Thread.Sleep(1000);
                }

                if (i == (time - 1))
                {
                    msg = "Тест не был пройден за отведённое время";
                    status = "Timeout";
                    logger.Info("Тест не был пройден за отведённое время: " + time + " сек.");
                }
            }
        }

        public void CheckDir(out string msg, out string status)
        {
            var logger = NLog.LogManager.GetCurrentClassLogger();
            status = "";
            msg = "";

            for (int i = 0; i < 10; i++) //Проверяем, создалась ли директория логов теста
            {
                if (Directory.Exists(scritp_logs_dir))
                {
                    status = "OK";
                    msg = "Идёт выполнение теста";
                    logger.Info("Директория результатов теста найдена");
                    break;
                }
                else
                {
                    msg = "Не удалось найти директорию с логами теста";
                    logger.Error("Попытка №" + (i+1) + ". Не удалось найти директорию с логами теста: " + scritp_logs_dir);
                    status = "Failed";
                    logger.Error("Попытка №" + (i + 1) + ". Не удалось найти директорию с логами теста: " + scritp_logs_dir);
                }
            }
        }
    }
}
