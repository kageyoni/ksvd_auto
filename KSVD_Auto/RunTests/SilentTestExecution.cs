﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace KSVD_Auto
{
    class SilentTestExecution
    {
        public void SilentTestExecutionStart(string testListPath, int time)
        {
            var logger = NLog.LogManager.GetCurrentClassLogger();
            logger.Info("Подготовка к запуску прогона");
            ImportFromXML importFromXML = new ImportFromXML(testListPath);
            logger.Info("Импорт списка тестов успешен");
            List<TestList> testList = importFromXML.DoImportFromXML(); //Импортируем список тестов
            logger.Info("Запуск прогона");

            string tmp = testList[0].FullPath;
            string scriptsDir = tmp.Remove(tmp.LastIndexOf('\\')); //Удаляем все символы с конца до первого \\ включительно
            string resultsDir = scriptsDir + "\\Results";

            if (Directory.Exists(resultsDir))
            {
                try
                {
                    Directory.Delete(resultsDir, true);
                    logger.Info("Удалена директория, содержащая предыдущие результаты выполнения скриптов: " + resultsDir);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
            }

            for (int i = 0; i < testList.Count; i++) //Основной цикл выполнения скриптов
            {
                logger.Info("Выполняется тест №" + i + ". Имя теста: " + testList[i].Name);
                string status = "";
                string msg = "";
                string scritp_logs_dir = scriptsDir + "\\Results\\" + testList[i].Name;
                string success_file = scritp_logs_dir + "\\success.txt";
                string fail_file = scritp_logs_dir + "\\fail.txt";
                RunAndCheckTestExecution startExecutingTests = new RunAndCheckTestExecution(scritp_logs_dir, success_file, fail_file);

                startExecutingTests.RunTests(testList[i].FullPath, time, out status, out msg); //Запускаем тест

                if (status == "OK") //Проверка запуска
                {
                    startExecutingTests.CheckDir(out msg, out status); //Запускаем проверку, создал ли тест директорию логов и скринов

                    if (status == "OK")
                    {
                        testList[i].Msg = msg;
                        startExecutingTests.CheckSuccessFileAndFailFile(time, out msg, out status); //Запускаем проверку статуса теста по файлам success.txt и error.txt
                    }
                }
                if (status == "OK") //Финальная проверка статуса выполнения теста
                {
                    testList[i].Status = status;
                    testList[i].Msg = msg;
                    logger.Info("Тест: " + testList[i].Name + " успешно завершен");
                }
                else if (status == "Timeout")
                {
                    testList[i].Status = status;
                    testList[i].Msg = msg;
                    try
                    {
                        foreach (Process proc in Process.GetProcessesByName("AutoIt3"))
                        {
                            proc.Kill();
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex);
                    }
                }
                else
                {
                    testList[i].Status = status;
                    testList[i].Msg = msg;
                    logger.Info("Тест: " + testList[i].Name + " завершен с ошибкой");
                }
            }

            //Экспортируем результат прогона в HTML
            logger.Info("Экспорт результатов прогона в HTML");
            string filename = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\results\\results.html"; 
            string[] columns_header = new string[5];
            columns_header[0] = "";
            columns_header[1] = "Очерёдность";
            columns_header[2] = "Имя теста";
            columns_header[3] = "Статус";
            columns_header[4] = "Сообщение";
            ListToHTML listToHTML = new ListToHTML(columns_header.Count(), columns_header, testList);
            string html_body = listToHTML.StartConvertListToHTML();
            File.WriteAllText(@filename, html_body);
            logger.Info("Экспорт результатов прогона в HTML успешно завершен");
        }
    }
}
