﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Windows;

namespace KSVD_Auto
{
    public partial class MainWindow : Window
    {
        private void ExecutingRun(int time) // Поток выполнения тестов
        {

            bool stopTestsExecution = false;
            Properties.Settings.Default.StopTestsExecution = false;

            var logger = NLog.LogManager.GetCurrentClassLogger(); //Подключение логгера NLog
            Dispatcher.BeginInvoke(new ThreadStart(delegate { tests_data_grid.IsEnabled = false; }));
            Dispatcher.BeginInvoke(new ThreadStart(delegate { up_btn.IsEnabled = false; }));
            Dispatcher.BeginInvoke(new ThreadStart(delegate { down_btn.IsEnabled = false; }));
            Dispatcher.BeginInvoke(new ThreadStart(delegate { delete_btn.IsEnabled = false; }));
            Dispatcher.BeginInvoke(new ThreadStart(delegate { cancel_btn.IsEnabled = true; }));
            Dispatcher.BeginInvoke(new ThreadStart(delegate { start_btn.IsEnabled = false; }));

            logger.Info("Выполняется установка статуса тестам, участвующим в прогоне");
            for (int i = 0; i < tests_data_grid.Items.Count; i++) // Проставляем тестам статус "В очереди"
            {

                if (testList[i].CheckBox == true)
                {
                    testList[i].Status = "InQueue";
                    testList[i].Msg = "";
                }
                else
                {
                    testList[i].Status = "Не участвует в прогоне";
                    testList[i].Msg = "";
                }
            }
            logger.Info("Установка статусов выполнена");

            logger.Info("Запускается основной цикл выполнения тестов");
            for (int i = 0; i < tests_data_grid.Items.Count; i++) // Основной цикл выполнения скриптов
            {
                stopTestsExecution = Properties.Settings.Default.StopTestsExecution;

                if (stopTestsExecution == false)
                {
                    if (testList[i].CheckBox == true)
                    {
                        string status = "";
                        string msg = "";
                        string scritpLogsDir = scriptsDir + "\\Results\\" + testList[i].Name;
                        string success_file = scritpLogsDir + "\\success.txt";
                        //string inprogress_file = scritp_logs_dir + "\\inprogress.txt";
                        string fail_file = scritpLogsDir + "\\fail.txt";
                        string shared_config_file = scriptsDir + "\\" + "config.ini";
                        string script_config_file = scriptsDir + "\\" + testList[i].Name + ".ini";

                        logger.Info("Подготовка к локальному выполнению теста");
                        testList[i].Status = "InProgress";
                        testList[i].Msg = "Тест выполняется";
                        Dispatcher.BeginInvoke(new ThreadStart(delegate { tests_data_grid.Items.Refresh(); })); //Перерисовываем DataGrid

                        LocalTestExecution localTestExecution = new LocalTestExecution(scriptsDir, testList[i].FullPath, testList[i].Name, time);
                        localTestExecution.DoLocalTestExecution(out status, out msg); //Запускается локальное выполнение теста, по окончанию отдаются результаты

                        if (status == "OK") //Финальная проверка статуса выполнения теста
                        {
                            testList[i].Status = status;
                            testList[i].Msg = msg;
                        }
                        else
                        {
                            testList[i].Status = status;
                            testList[i].Msg = msg;
                        }

                        testList[i].CheckBox = false; //Возвращаем статус чекбокса в таблице на false, т.к. DataGrid после перерисовки сбрасывает чекбокс, но не меняет его статус в List<TestList>, что приводит к ошибкам
                    }
                }
                else if (stopTestsExecution == true)
                {
                    logger.Info("Проставление статусов отменённым тестам");

                    for (int y = 0; y < tests_data_grid.Items.Count; y++) //Проставление статуса отменённым тестам
                    {
                        testList[y].CheckBox = false;


                        if (testList[y].Status == "InQueue")
                        {
                            testList[y].Status = "Cancelled";
                            testList[y].Msg = "Выполнение прогона остановлено";
                        }
                    }
                    break; //Отменяется основной цикл выполнения скриптов
                }
            }
            Dispatcher.BeginInvoke(new ThreadStart(delegate { cancel_btn.IsEnabled = false; }));
            Dispatcher.BeginInvoke(new ThreadStart(delegate { tests_data_grid.Items.Refresh(); }));
            Dispatcher.BeginInvoke(new ThreadStart(delegate { export_html_btn.IsEnabled = true; }));
            Dispatcher.BeginInvoke(new ThreadStart(delegate { tests_data_grid.IsEnabled = true; }));
        }
    }
}
