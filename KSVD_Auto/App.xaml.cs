﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace KSVD_Auto
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            string[] args = e.Args;
            int count = args.Count();
            var logger = NLog.LogManager.GetCurrentClassLogger(); //Подключение логгера NLog
            logger.Info("Запуск");


            if (args.Count() == 0) //если не введены аргументы
            {
                logger.Info("Запуск без аргументов");
                new MainWindow().ShowDialog();
            }
            else if (args.Count() != 5) //если кол-во введённых аргументов не соответствует ожидаемому
            {
                logger.Fatal("Некорректный ввод аргументов!");
            }
            else if (args.Count() == 5) // если кол - во введённых аргументов соответствует ожидаемому
            {
                if (args[0] != "/silent" & args[1] != "/testList" & args[2] != "" & args[3] != "/time" & args[4] != "") //Проверка на правильность ввода аргументов
                {
                    logger.Fatal("Некорректный ввод аргументов!");
                    this.Shutdown();
                }
                else
                {
                    logger.Info("Запуск в тихом режиме");
                    string testListPath = args[2];
                    int time = Convert.ToInt32(args[4]);
                    SilentTestExecution ste = new SilentTestExecution();
                    ste.SilentTestExecutionStart(testListPath, time);
                    logger.Info("Прогон тестов завершен");
                    this.Shutdown();
                }
            }
            else
            {
                logger.Fatal("Некорректный ввод аргументов!");
                this.Shutdown();
            }
        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            var logger = NLog.LogManager.GetCurrentClassLogger(); //Подключение логгера NLog
            logger.Error("An unhandled exception just occurred: " + e.Exception.Message);
            e.Handled = true;
        }
    }
}
